<?php
 include "koneksi.php";
 include "create_message.php";

 if (isset($_GET['mahasiswa_id'])) {
    $mahasiswa_id = $_GET['mahasiswa_id'];

    // Dapatkan nama file foto dari database
    $sql_get_foto = "SELECT foto FROM mahasiswa WHERE mahasiswa_id = '$mahasiswa_id'";
    $result = $conn->query($sql_get_foto);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $foto_path = "uploads/" . $row['foto'];

        // Hapus foto dari penyimpanan
        if (file_exists($foto_path)) {
            unlink($foto_path);
        }

        // Hapus data mahasiswa dari database
        $sql_delete_mahasiswa = "DELETE FROM mahasiswa WHERE mahasiswa_id = '$mahasiswa_id'";
        if ($conn->query($sql_delete_mahasiswa) === TRUE) {
            $conn->close();
            create_message("Hapus Data Berhasil", "success", "check");
            header("location:" . $_SERVER['HTTP_REFERER']);
            exit();
        } else {
            $conn->close();
            create_message("Error: " . $sql_delete_mahasiswa . "<br>" . $conn->error, "danger", "warning");
            header("location:" . $_SERVER['HTTP_REFERER']);
            exit();
        }
    } else {
        create_message("Data Mahasiswa tidak ditemukan", "danger", "warning");
        header("location:" . $_SERVER['HTTP_REFERER']);
        exit();
    }
} else {
    create_message("Parameter tidak valid", "danger", "warning");
    header("location:" . $_SERVER['HTTP_REFERER']);
    exit();
}
?>