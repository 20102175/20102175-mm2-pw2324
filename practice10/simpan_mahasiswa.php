<?php
include "koneksi.php";
include "create_message.php";

$upload_dir = "uploads/";
$imageFileType = strtolower(pathinfo($_FILES["gambar_contoh"]["name"], PATHINFO_EXTENSION));

if (isset($_POST['mahasiswa_id'])) {
    // Kondisi Update
    $sql = "UPDATE mahasiswa SET nama_lengkap = '" . $_POST['nama_lengkap'] . "', alamat= '" . $_POST['alamat'] . "', kelas_id = '" . $_POST['kelas_id'] . "' WHERE mahasiswa_id = '" . $_POST['mahasiswa_id'] . "'";
    if ($conn->query($sql) === TRUE) {
        $conn->close();
        create_message("Ubah Data Berhasil", "success", "check");
        header("location:" . $_SERVER['HTTP_REFERER']);
        exit();
    } else {
        $conn->close();
        create_message("Error: " . $sql . "<br>" . $conn->error, "danger", "warning");
        header("location:" . $_SERVER['HTTP_REFERER']);
        exit();
    }
} else {
    // Kondisi Insert
    $foto = isset($_FILES["gambar_contoh"]["name"]) ? $_FILES["gambar_contoh"]["name"] : ""; // Ambil nama file gambar jika ada
    $target_file = $upload_dir . $last_id . "" . basename($_FILES["gambar_contoh"]["name"]); // Definisikan target_file dengan nama file yang sesuai
    $sql = "INSERT INTO mahasiswa (nama_lengkap, kelas_id, alamat, foto) VALUES ('" . $_POST['nama_lengkap'] . "', " . $_POST['kelas_id'] . ", '" . $_POST['alamat'] . "', '$foto')";

    if ($conn->query($sql) === TRUE) {
        $last_id = $conn->insert_id;
        $conn->close();

        if (!empty($_FILES["gambar_contoh"]["name"])) {
            $check = getimagesize($_FILES["gambar_contoh"]["tmp_name"]);
            if ($check === false) {
                create_message("File bukan gambar.", "danger", "warning");
                header("location:" . $_SERVER['HTTP_REFERER']);
                exit();
            }

            $allowed_formats = ["jpg", "jpeg", "png", "gif"];
            if (!in_array($imageFileType, $allowed_formats)) {
                create_message("Maaf, hanya file JPG, JPEG, PNG & GIF yang diizinkan.", "danger", "warning");
                header("location:" . $_SERVER['HTTP_REFERER']);
                exit();
            }

            if ($_FILES["gambar_contoh"]["size"] > 500000) {
                create_message("Maaf, ukuran file terlalu besar.", "danger", "warning");
                header("location:" . $_SERVER['HTTP_REFERER']);
                exit();
            }

            if (move_uploaded_file($_FILES["gambar_contoh"]["tmp_name"], $target_file)) {
                create_message("Simpan Data Berhasil", "success", "check");
                header("location:index.php");
                exit();
            } else {
                create_message("Maaf, terjadi kesalahan saat mengupload file.", "danger", "warning");
                header("location:" . $_SERVER['HTTP_REFERER']);
                exit();
            }
        } else {
            create_message("Simpan Data Berhasil (tanpa gambar)", "success", "check");
            header("location:index.php");
            exit();
        }
    } else {
        $conn->close();
        create_message("Error: " . $sql . "<br>" . $conn->error, "danger", "warning");
        header("location:index.php");
        exit();
    }
}